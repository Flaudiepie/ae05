using System;
using Xunit;
using AE_05_SDLC;
using System.Collections.ObjectModel;
using System.Numerics;
using System.IO;

namespace Testing
{
    public class Testing
    {
        // [Fact (Skip="Wird nicht mehr benötigt")]
        // public void StartTest()
        // {
        //     Assert.True(false);
        // }

        // [Fact (Skip="ConsolenEingabe nicht möglich per Test. Durch Umwege bestanden")]
        // public void UserInput()
        // {
        //     FrontEnd frontEnd = new FrontEnd();

        //     string eingabe = frontEnd.NutzerEingabe();

        //     Assert.NotNull(eingabe);
        // }

        [Fact]
        public void ObjektErstellungUndZuweisung()
        {
            BackEnd backEnd = new BackEnd();
            ObservableCollection<BigInteger> liste = backEnd.ErstelleListe();
            Assert.NotNull(liste);
        }

        [Fact]
        public void ZahlenBerechnen()
        {
            BackEnd backEnd = new BackEnd();
            var liste = backEnd.ErstelleListe();
            liste = backEnd.BerechneFibonacciZahlen(10, liste);
            Assert.Equal(10, liste.Count);
        }

        [Fact]
        public void ZahlenAusgeben()
        {
            FrontEnd frontEnd = new FrontEnd();
            BackEnd backEnd = new BackEnd();
            var liste = backEnd.ErstelleListe();
            liste = backEnd.BerechneFibonacciZahlen(10, liste);
            frontEnd.AusgabeVonZahlen(liste);
            Assert.True(true);
        }

        [Fact]
        public void TesteProgrammBeginn()
        {
            BackEnd backEnd = new BackEnd();
            backEnd.StarteAnwendung(5);
            Assert.True(true);
        }

        [Fact]
        public void LoggingTest()
        {
            Logging.AddLog("Testing", LogLevel.Debug);
            Assert.True(Logging.CreateLogFile()); 
        }

        //[Fact (Skip="Test wird übersprungen, da Validator als private deklariert wurde! Wurde erfolgreich getestet")]
        //public void TestValidator()
        //{
         //   FrontEnd frontEnd = new FrontEnd();
         //   Assert.False(frontEnd.Validator("1000", out var x));
        //}
    }
}
