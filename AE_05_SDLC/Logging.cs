using System;
using System.Collections.Generic;
using System.IO;

namespace AE_05_SDLC
{
    public static class Logging
    {
        private static List<string> Logs = new List<string>();

        static Logging()
        {
            AddLog("Process started", LogLevel.Info);
        }

        public static void AddLog(string statement, LogLevel logLevel)
        {
            string log = $"{DateTime.Now} [{logLevel}] - {statement}";
            Logs.Add(log);
            Console.WriteLine(log);
        }

        public static bool CreateLogFile()
        {
            try
            {
                AddLog("Generating Logfile...", LogLevel.Info);

                Directory.CreateDirectory($"{Directory.GetCurrentDirectory()}/Logs");

                string path = $"{Directory.GetCurrentDirectory()}/Logs/log{DateTime.Now.ToString("dd_MM_yyyy__hh_mm")}.txt";

                using (StreamWriter sw = new StreamWriter(path))
                {
                    foreach (var log in Logs)
                    {
                        sw.WriteLine(log);
                    }
                }
                return true;
            }
            catch(Exception e)
            {
                Logging.AddLog($"Exception occurred! More details: {e}", LogLevel.Error);
                return false;
            }
        }
    }
}
