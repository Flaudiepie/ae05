using System;
using System.Collections.ObjectModel;
using System.Numerics;

namespace AE_05_SDLC
{
    public class FrontEnd
    {
        public FrontEnd()
        {
            Logging.AddLog("Frontend loaded", LogLevel.Debug);
        }

        public int WieVieleZahlenZuBerechnen()
        {
            string eingabe;
            int ausgabe;

            do
            {
                Logging.AddLog("Wie viele Fibonaccizahlen wollen Sie berechnet haben? Max. Value: 999", LogLevel.Info);
                Console.Write("Eingabe: ");
                eingabe = Console.ReadLine().Trim();
            }
            while (!Validator(eingabe, out ausgabe));

            Logging.AddLog($"User entered value: \"{eingabe}\"", LogLevel.Info);

            return ausgabe;
        }

        public void AusgabeVonZahlen(ObservableCollection<BigInteger> liste)
        {
            Logging.AddLog("Returning results...", LogLevel.Info);
            int counter = 0;
            foreach (var x in liste)
            {
                 Console.WriteLine($"{counter}:\t{x}");
                 counter++;
            }
        }

        private bool Validator(string eingabe, out int ausgabe)
        {
            ausgabe = 0;
            if(Int32.TryParse(eingabe, out var zahl))
            {
                if(zahl > 0 && zahl < 999)
                {
                    Logging.AddLog("Userinput is valid!", LogLevel.Info);
                    ausgabe = zahl;
                    return true;
                }
                Logging.AddLog($"User entered wrong value: \"{eingabe}\"", LogLevel.Info);
                Logging.AddLog("Input is not in the valid range! Repeating process...", LogLevel.Warn);
                return false;
            }
            Logging.AddLog("Userinput was not a number!", LogLevel.Error);
            return false;
        }
    }
}
