﻿using System;

namespace AE_05_SDLC
{
    public enum LogLevel
    {
        Debug,
        Info,
        Warn,
        Error
    }
}
