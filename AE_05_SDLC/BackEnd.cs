using System;
using System.Collections.ObjectModel;
using System.Numerics;

namespace AE_05_SDLC
{
    public class BackEnd
    {
        public BackEnd()
        {
            Logging.AddLog("Backend loaded.", LogLevel.Debug);
        }

        public void StarteAnwendung()
        {
            Logging.AddLog("Application is launching normally.", LogLevel.Info);
            var liste = ErstelleListe();
            var frontEnd = new FrontEnd();
            var anzahl = frontEnd.WieVieleZahlenZuBerechnen();

            liste = BerechneFibonacciZahlen(anzahl, liste);
            frontEnd.AusgabeVonZahlen(liste);
            Logging.AddLog("Application finished!", LogLevel.Info);
        }

        public void StarteAnwendung(int anzahl)
        {
            Logging.AddLog("Application is launching for testing.", LogLevel.Info);
            var liste = ErstelleListe();
            var frontEnd = new FrontEnd();
            
            liste = BerechneFibonacciZahlen(anzahl, liste);
            frontEnd.AusgabeVonZahlen(liste);
            Logging.AddLog("Application finished!", LogLevel.Info);
        }

        public ObservableCollection<BigInteger> ErstelleListe()
        {
            Logging.AddLog("List prepared successfully!", LogLevel.Debug);
            ObservableCollection<BigInteger> liste = new ObservableCollection<BigInteger>();
            liste.Add(0);
            liste.Add(1);
            return liste;
        }

        public ObservableCollection<BigInteger> BerechneFibonacciZahlen(int anzahl, ObservableCollection<BigInteger> liste)
        {
            Logging.AddLog("Calculating...", LogLevel.Debug);
            BigInteger a = 0;
            BigInteger b = 1;
            BigInteger ergebnis = 0;
            while (liste.Count != anzahl)
            {
                ergebnis = a + b;
                a = b;
                b = ergebnis;
                liste.Add(ergebnis);
            }
            return liste;
        }
    }
}
