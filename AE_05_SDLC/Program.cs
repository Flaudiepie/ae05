using System;
using System.IO;

namespace AE_05_SDLC
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                BackEnd backEnd = new BackEnd();
                backEnd.StarteAnwendung();
            }
            catch(Exception e)
            {
                Logging.AddLog($"Exception occurred! More details: {e}", LogLevel.Error);
            }
            Logging.CreateLogFile();
        }
    }
}
